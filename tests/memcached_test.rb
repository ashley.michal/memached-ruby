require_relative './test_helper'

context "MemCached::Cache" do
  context "#set" do
    test "Stores a value and returns #{MemCached::Response::STORED}" do
      cache = MemCached::Cache.()
      key = "key"
      value = "value"

      response = cache.set(key, value)
      assert response.type == MemCached::Response::STORED
      assert cache.get(key) == value
    end
  end

  context "#add" do
    context "when the key does not yet exist" do
      test "sets the new value" do
        cache = MemCached::Cache.()
        key = "key"
        value = "value"

        response = cache.add(key, value)
        assert response.type == MemCached::Response::STORED
        assert cache.get(key) == value
      end
    end

    context "when the key already exists" do
      test "does not set the new value" do
        cache = MemCached::Cache.()
        key = "key"
        old_value = "old_value"

        cache.set(key, old_value)

        new_value = "new_value"

        response = cache.add(key, new_value)
        assert response.type == MemCached::Response::NOT_STORED
        assert cache.get(key) == old_value
      end
    end
  end

  context "#replace" do
    context "when the key does not yet exist" do
      test "does not set the value" do
        cache = MemCached::Cache.()
        key = "key"
        value = "value"

        response = cache.replace(key, value)
        assert response.type == MemCached::Response::NOT_STORED
        assert cache.get(key) == nil
      end
    end

    context "when the key already exists" do
      test "sets the new value" do
        cache = MemCached::Cache.()
        key = "key"
        old_value = "old_value"

        cache.set(key, old_value)

        new_value = "new_value"

        response = cache.replace(key, new_value)
        assert response.type == MemCached::Response::STORED
        assert cache.get(key) == new_value
      end
    end
  end

  context "#prepend" do
    context "when the key does not yet exist" do
      test "sets the value" do
        cache = MemCached::Cache.()
        key = "key"
        value = "value"

        response = cache.prepend(key, value)
        assert response.type == MemCached::Response::STORED
        assert cache.get(key) == value
      end
    end

    context "when the key already exists" do
      test "adds the new value to the beginning of the old value" do
        cache = MemCached::Cache.()
        key = "key"
        old_value = "old_value"

        cache.set(key, old_value)

        new_value = "new_value"

        response = cache.prepend(key, new_value)
        assert response.type == MemCached::Response::STORED
        assert cache.get(key) == "#{new_value}#{old_value}"
      end
    end
  end

  context "#append" do
    context "when the key does not yet exist" do
      test "sets the value" do
        cache = MemCached::Cache.()
        key = "key"
        value = "value"

        response = cache.append(key, value)
        assert response.type == MemCached::Response::STORED
        assert cache.get(key) == value
      end
    end

    context "when the key already exists" do
      test "adds the new value to the end of the old value" do
        cache = MemCached::Cache.()
        key = "key"
        old_value = "old_value"

        cache.set(key, old_value)

        new_value = "new_value"

        response = cache.append(key, new_value)
        assert response.type == MemCached::Response::STORED
        assert cache.get(key) == "#{old_value}#{new_value}"
      end
    end
  end
end
