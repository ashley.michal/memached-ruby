# Memcached

A small exercise in implementing a Memcache in Ruby. Based on the protocol described [here](https://github.com/memcached/memcached/blob/master/doc/protocol.txt)

## Cache Commands

### Storage Commands

`set` - means "store this data"
`add` - means "store this data, but only if the server _doesn't_ already hold data for this key".
`replace` - means "store this data, but only if the server _does_ already hold data for this key".
`prepend` - means "add this data to an existing key before existing data".
`append` - means "add this data to an existing key after existing data".

### Retrieval Commands

`get` - Responds with the data stored under a key
`gets` - means "get all entries for all these keys"

## Responses

`STORED` - Indicates success
`NOT_STORED` - Indicates the data was not stored, but not because of an error. This normally means that the condition for an "add" or a "replace" command was not met.
`EXISTS` - Indicates that the item you are trying to store with a "cas" command has been modified since you last fetched it.
`NOT_FOUND` - Indicates that the item you are trying to store with a "cas" command did not exist.

## Errors

`ERROR` - means the client sent a nonexistent command name.
`CLIENT_ERROR <message>` - means some sort of client error in the input line, i.e. the input doesn't conform to the protocol in some way. <message> is a human-readable error string.
`SERVER_ERROR <message>` - means some sort of server error prevents the server from carrying out the command. <message> is a human-readable error string. In cases of severe server errors, which make it impossible to continue serving the client (this shouldn't normally happen), the server will close the connection after sending the error line. This is the only case in which the server closes a connection to a client.

## Other important details

All lines should end with \r\n

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'memcached'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install memcached

## Usage

TODO: Write usage instructions here

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/memcached.
