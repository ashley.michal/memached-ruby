module MemCached
  class Command
    STORAGE_COMMANDS = [
      :set,
      :add,
      :replace,
      :append,
      :prepend,
    ]
    RETRIEVAL_COMMANDS = [
      :get,
      :gets,
    ]

    def initialize(name, key, flags, exptime, bytes, no_reply, payload)
      @name = name
      @key = key
      @flags = flags
      @exptime = exptime
      @bytes = bytes
      @no_reply = no_reply
      @payload = payload
    end

    def self.store(command_name, command, payload)
      key, flags, exptime, bytes, no_reply = command.split
      initialize(command_name, key, flags, exptime, bytes, no_reply, payload)
    end

    def self.retrieve(command_name, keys)

    end

    ##
    # Clients send requests with a specific format, starting with the
    # command, followed by the key, and (for some commands) a set of options,
    # including flags, TTL in seconds, and the size in bytes of the incoming
    # data.
    # TODO: add cas
    def self.call command_string
      command, payload = command_string.split LINE_END
      command_name, command_params = command.split(nil, 2)

      return self.store(command_name, command_params, payload) if STORAGE_COMMANDS.include? command_name
      return self.retrieve(command_name, command_params) if RETRIEVAL_COMMANDS.include? command_name

      Error.(Error::ERROR)
    end
  end
end
