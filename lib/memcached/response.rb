module MemCached
  class Response
    STORED = "STORED"
    NOT_STORED = "NOT_STORED"
    EXISTS = "EXISTS"
    NOT_FOUND = "NOT_FOUND"

    attr_reader :type

    def initialize(type)
      @type = type
    end

    def self.call(type)
      new(type)
    end

    def to_s
      "#{@type}#{LINE_END}"
    end

    def call
      to_s
    end
  end
end
