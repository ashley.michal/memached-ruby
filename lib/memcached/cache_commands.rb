module MemCached
  module CacheCommands
    def add(key, data)
      return Response.(Response::NOT_STORED) if self.get(key)

      self.set(key, data)
    end

    def replace(key, data)
      return Response.(Response::NOT_STORED) unless self.get(key)

      self.set(key, data)
    end

    def append(key, data)
      old_data = String.new(self.get(key) || '')

      self.set(key, old_data.concat(data))
    end

    def prepend(key, data)
      old_data = String.new(self.get(key) || '')

      self.set(key, old_data.insert(0, data))
    end

    def gets *keys
      keys.map { |key| self.get(key) }
    end
  end
end
