module MemCached
  class Cache
    include CacheCommands

    def initialize(cache)
      @cache = cache
    end

    def self.call
      new(Hash.new)
    end

    def set(key, data)
      new_cache = Hash.new(@cache)
      @cache = new_cache.merge({ key => data })

      Response.(Response::STORED)
    end

    ##
    #
    def get key
      @cache[key]
    end
  end
end
