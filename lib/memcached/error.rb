module MemCached
  class Error
    ERROR = "ERROR"
    CLIENT_ERROR = "CLIENT_ERROR"
    SERVER_ERROR = "SERVER_ERROR"

    messages = {
      ERROR: -> () { '' },
      CLIENT_ERROR: -> (reason) { "Error reading input: #{reason}" },
      SERVER_ERROR: -> (reason) { "Internal error: #{reason}" },
    }

    def initialize(type, message)
      @type = type
      @message = message
    end

    def self.call(type, reason = nil)
      message = messages[type].(reason)
      new(type, message)
    end

    def to_s
      "#{@type} #{@message}#{LINE_END}"
    end

    def call
      to_s
    end
  end
end
